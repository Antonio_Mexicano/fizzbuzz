// Test para FizzBuzz

describe('Pruebas de fizzBuzz', function (){

    describe('fizz test', function () {
        it ('9 is divisible by 3, 9 is a "Fizz" number', fizz = () => {
            let testNumber = 9
                result = fizzNumber(testNumber)
            expect (result). toEqual ('fizz')
        })

        it ("98 isn't divisible by 3, 98 is not a 'Fizz' number", fizz = () => {
            let testNumber = 98
                result = fizzNumber(testNumber)
            expect (result). toEqual (98)
        })
    }) 

    describe('buzz test', function () {
        it ('10 is divisible by 5, 10 is a "buzz" number', buzz = () => {
            let testNumber = 10
                result = buzzNumber(testNumber)
            expect (result). toEqual ('buzz')
        })

        it (" 99 isn't divisible by 5, 99 is not a 'buzz' number", buzz = () => {
            let testNumber = 99
                result = buzzNumber(testNumber)
            expect (result). toEqual (99)
        })
    }) 

    describe('fizzbuzz test', function () {
        it ('15 is divisible by 3 and by 5, 15 is a "fizzbuzz" number', fizzbuzz = () => {
            let testNumber = 15
                result = fizzbuzzNumber(testNumber)
            expect (result). toEqual ('fizzbuzz')
        })
        
        it ('18 is fizz but not buzz, so it is NOT a "fizzbuzz" number', fizzbuzz = () => {
            let testNumber = 18
                result = fizzbuzzNumber(testNumber)
            expect (result). toEqual ('no es fizzbuzz')
        })
    
    })


    describe('Not fizz, or buzz or fizzbuzz test',function() {
       it ('17 is NOT "fizz, buzz or fizzbuzz", so log the number', justANumber = () => {
            let testNumber = 17
                result = isANumber(testNumber)
            expect (result). toEqual (testNumber)
        }) 

    })

})
  